package com.atos.redhat.springboot.microservices.cardpaymentservice.services;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;
import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CustomerInfo;

@Component
public class CreditCardValidatorServiceImpl implements CreditCardValidatorService {

	@Override
	public boolean isValid(CustomerInfo customerInfo, CreditCardInfo cardInfo) {

		if (customerInfo != null & cardInfo != null) {

			if (!StringUtils.isEmpty(customerInfo.getFirstName()) && !StringUtils.isEmpty(cardInfo.getCardNo())) {

				if (customerInfo.getFirstName().equalsIgnoreCase("Aswini Kumar")
						&& cardInfo.getCardNo() == 1111222233334444l && cardInfo.getSecurityCode() == 210 ) {
					return true;
				}
			}
		}

		return false;
	}

}
