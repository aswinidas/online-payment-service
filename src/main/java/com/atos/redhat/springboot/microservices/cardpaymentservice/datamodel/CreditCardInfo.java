package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.util.Date;

public class CreditCardInfo {

	private Long cardNo;
	private int securityCode;
	private Date expriryDate;
	
	public Long getCardNo() {
		return cardNo;
	}
	public void setCardNo(Long cardNo) {
		this.cardNo = cardNo;
	}
	public int getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}
	public Date getExpriryDate() {
		return expriryDate;
	}
	public void setExpriryDate(Date expriryDate) {
		this.expriryDate = expriryDate;
	}
	
	
}
