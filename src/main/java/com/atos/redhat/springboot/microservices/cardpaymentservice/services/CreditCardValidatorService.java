package com.atos.redhat.springboot.microservices.cardpaymentservice.services;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;
import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CustomerInfo;

public interface CreditCardValidatorService {

	public boolean isValid(CustomerInfo customerInfo, CreditCardInfo cardInfo);
}
