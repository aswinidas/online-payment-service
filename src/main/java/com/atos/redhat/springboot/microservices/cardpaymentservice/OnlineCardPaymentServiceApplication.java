package com.atos.redhat.springboot.microservices.cardpaymentservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineCardPaymentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineCardPaymentServiceApplication.class, args);
	}
}
