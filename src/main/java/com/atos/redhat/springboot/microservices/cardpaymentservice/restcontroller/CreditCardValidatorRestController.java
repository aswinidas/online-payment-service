package com.atos.redhat.springboot.microservices.cardpaymentservice.restcontroller;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.atos.redhat.springboot.microservices.cardpaymentservice.services.CreditCardValidatorService;

@RestController
public class CreditCardValidatorRestController {

	@Autowired
	CreditCardValidatorService	creditCardValidatorService;
	
	@PostMapping("/creditcardservice/validate")
	public CreditCardValidatorServiceResponse validate( @RequestBody CreditCardValidatorServiceRequest creditCardrequest) {
		
		System.out.println("Data received from Camel routing engine="+creditCardrequest.getRequestId());
		CreditCardValidatorServiceResponse response = new CreditCardValidatorServiceResponse();
		response.setCardInfo(creditCardrequest.getCardInfo());
		UUID transactionId = UUID.randomUUID();  
		response.setServiceTransactionId(transactionId.toString());
		
		if(creditCardValidatorService.isValid(creditCardrequest.getCustomerInfo(), creditCardrequest.getCardInfo())) 
			response.setValid(true);
		
		return response;
	}
	
	@GetMapping("/service/test")
	public String test() {
		return "Hello Aswini Das";
	}
}
