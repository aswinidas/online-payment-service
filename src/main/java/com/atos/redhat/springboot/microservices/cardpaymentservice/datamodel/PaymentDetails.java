package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

public class PaymentDetails {

	private String paymentId;
	private CustomerInfo customerInfo;
	private CreditCardInfo creditCardInfo;
}
