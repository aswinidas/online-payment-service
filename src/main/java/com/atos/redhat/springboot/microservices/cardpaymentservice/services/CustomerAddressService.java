package com.atos.redhat.springboot.microservices.cardpaymentservice.services;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.Address;

public interface CustomerAddressService {
	public Address getCustomerAddress(String customerId, String addressType);
}
