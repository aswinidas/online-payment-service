package com.atos.redhat.springboot.microservices.cardpaymentservice.restcontroller;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;
import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CustomerInfo;

public class CreditCardValidatorServiceRequest {

	private String requestId;
	private CreditCardInfo cardInfo;
	private CustomerInfo customerInfo;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public CreditCardInfo getCardInfo() {
		return cardInfo;
	}
	public void setCardInfo(CreditCardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}
	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	
	
	
}
