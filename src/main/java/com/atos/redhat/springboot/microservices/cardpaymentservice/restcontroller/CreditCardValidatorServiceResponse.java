package com.atos.redhat.springboot.microservices.cardpaymentservice.restcontroller;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;

public class CreditCardValidatorServiceResponse {

	private String serviceTransactionId;
	private boolean isValid;
	private CreditCardInfo cardInfo;
	
	
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public CreditCardInfo getCardInfo() {
		return cardInfo;
	}
	public void setCardInfo(CreditCardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}
	public String getServiceTransactionId() {
		return serviceTransactionId;
	}
	public void setServiceTransactionId(String serviceTransactionId) {
		this.serviceTransactionId = serviceTransactionId;
	}
	
	
}
